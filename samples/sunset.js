class StylePair {
    constructor(style1, style2) {
        this.style1 = style1;
        this.style2 = style2;
    }
}

class Sun extends Circle {
    constructor(context, x, y, radius, direction, speed, angle, rotation) {
        let fillStyle = new RgbaColor(255, 255, 235);
        // let shadow = null;
        super(context, x, y, radius, direction, speed, angle, rotation, fillStyle);
        this.shadow = new Shadow(context, 0, 0, fillStyle, 12);
    }
}

class TowerLayer extends CanvasObject {
    constructor(context, y, fillStyle) {
        super(context, 0, y, 0, 0, 0, 0, fillStyle);
        this.towerPoints = createTowerPoints(context.canvas, y);
    }

    draw() {
        Shadow.setShadow(this.context, Math.round(this.context.canvas.width * .002), -Math.round(this.context.canvas.height * .002), 'white');
        for (let i = 0; i < this.towerPoints.length; i++) {
            drawPolygon(this.context, this.towerPoints, this.fillStyle);
        }
        Shadow.removeShadow(this.context);
    }
}

class Plane extends Triangle {
    constructor(context, y, size, isLeftDirection, speed, fillStyle, trailFillStyle) {
        let x = isLeftDirection ? context.canvas.width + size - 1 : -size + 1;
        let direction = isLeftDirection ? Math.PI : 0;
        super(context, x, y, size, direction, speed, direction, 0, fillStyle);
        this.trailFillStyle = trailFillStyle;
        this.trailLength = this.size * 50;
    }

    isLeft() {
        return this.direction === Math.PI;
    }

    trailPoints() {
        let points = [];

        let isLeft = this.isLeft();

        let startOffsetX = isLeft ? this.size : -this.size;

        // start point
        let startPoint = new Point(this.x + startOffsetX, this.y);

        let middleOffsetY = this.size * .5;
        let middleOffsetX = this.isLeft() ? this.trailLength : -this.size * 50;

        // middle points
        let middlePointTop = new Point(this.x + middleOffsetX, this.y - middleOffsetY);
        let middlePointBottom = new Point(this.x + middleOffsetX, this.y + middleOffsetY);


        let endX = isLeft ? this.context.canvas.width : 0;

        // end points
        let endPointTop = new Point(endX, middlePointTop.y);
        let endPointBottom = new Point(endX, middlePointBottom.y);

        points.push(startPoint);
        points.push(middlePointTop);
        points.push(endPointTop);
        points.push(endPointBottom);
        points.push(middlePointBottom);

        return points;
    }

    drawTrail() {
        drawPolygon(this.context, this.trailPoints(), this.trailFillStyle, null, true);
    }

    draw() {
        this.drawTrail();
        super.draw();
    }

    // isInBounds(boundsCheck = CanvasObject.bounds.whole) {
    //     let endPoints = this.trailEndPoints();
    //
    //     if (boundsCheck === CanvasObject.bounds.whole) {
    //         return super.isInBounds(boundsCheck) || isInBoundsPoint(this.context.canvas, endPoints.top) ||
    // isInBoundsPoint(this.context.canvas, endPoints.bottom); }  return super.isInBounds(boundsCheck) &&
    // isInBoundsPoint(this.context.canvas, endPoints.top) && isInBoundsPoint(this.context.canvas, endPoints.bottom); }
}

class Scene {
    constructor(context) {
        this.context = context;
        this.canvas = this.context.canvas;
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.horizon = this.height * .92;
        this.sun = new Sun(this.context, this.width * .89, this.height * .21, Math.round(this.height * .2), 0, 0, 0, 0);
        this.towerLayers = createTowerLayers(this.context, 2, this.horizon);
        this.skyGradient = createSkyGradient(this.context, this.horizon);
        // this.towerGradient = createTowerGradient(this.animationContext, this.width, this.horizon);
    }

    draw() {
        this.context.fillStyle = this.skyGradient;
        this.context.fillRect(0, 0, this.width, this.horizon);

        for (let i = 0; i < this.towerLayers.length; i++) {
            this.towerLayers[i].update();
        }

        // Shadow.setShadow(this.animationContext, -Math.round(this.width * .002), -Math.round(this.height * .002),
        // 'white'); for (let i = 0; i < this.towerLayers.length; i++) { drawPolygon(this.animationContext,
        // this.towerLayers[i], this.towerGradient); } Shadow.removeShadow(this.animationContext);
    }

    update() {
        this.draw();
        this.sun.update();
    }
}

class AnimationScene {
    constructor(context) {
        this.context = context;
        this.canvas = this.context.canvas;
        this.planes = [];
        this.planeSpawnIntervalFrames = msToFrames(5000);
        this.frame = 0;
    }

    addPlanes(planes = 1, isLeftDirection = randomBoolean()) {
        for (let i = 0; i < planes; i++) {
            let size = this.canvas.height * randomFloatInRange(.004, .005);
            let y = randomIntInRange(size, this.canvas.height * .3 - size);
            let speed = size * .05;
            let fillStyle = new RgbaColor(70, 70, 70);
            let trailFillStyle = fillStyle.toRgba();
            trailFillStyle.alpha = .2;

            this.planes.push(new Plane(this.context, y, size, isLeftDirection, speed, fillStyle, trailFillStyle));
        }
    }

    update() {
        if (this.planes.length === 0 && this.frame % this.planeSpawnIntervalFrames === 0) {
            this.addPlanes();
        }

        for (let i = 0; i < this.planes.length; i++) {
            let plane = this.planes[i];
            plane.update();

            if (!plane.isInBounds()) {
                this.planes.splice(i, 1);
                i--;
            }
        }

        this.frame++;
    }
}

function createSkyGradient(context, horizon) {
    let skyGradient = context.createLinearGradient(0, 0, context.canvas.width * .8, horizon);

    skyGradient.addColorStop(0, 'rgb(255, 5, 85)');
    skyGradient.addColorStop(.6, 'orange');
    skyGradient.addColorStop(1, 'rgb(255, 230, 100)');

    return skyGradient;
}

function createTowerGradient(context, width, horizon, style1, style2, colorStop1 = 0, colorStop2 = 1) {
    let verticalOffset = Math.round(horizon * .2);
    let middleHorizon = Math.round(horizon / 2);
    let towerGradient = context.createLinearGradient(0, middleHorizon - verticalOffset, width, middleHorizon + verticalOffset);

    towerGradient.addColorStop(colorStop1, getStyle(style1));
    towerGradient.addColorStop(colorStop2, getStyle(style2));

    return towerGradient;
}

function createTowerPoints(canvas, y) {
    let minWidth = Math.round(canvas.width * .015);
    let maxWidth = Math.round(canvas.width * .018);

    let minHeight = Math.round(y * .1);
    let maxHeight = Math.round(y * .2);

    let minSpacing = Math.round(canvas.width * .002);
    let maxSpacing = Math.round(canvas.width * .003);


    let points = [];
    let x = randomIntInRange(-maxWidth, 0, true);

    while (x < canvas.width) {
        let width = randomIntInRange(minWidth, maxWidth);
        let height = randomIntInRange(minHeight, maxHeight);
        // add random spacing
        x += randomIntInRange(minSpacing, maxSpacing);
        let leftHeightOffset = 0;
        let rightHeightOffset = 0;

        // random decision for tower slope
        if (randomFloat(1) < .2) {
            let slopeHeightOffset = randomIntInRange(width * .8, width * 1.5,);

            // left or right side offset
            if (randomInt(2) === 0) {
                leftHeightOffset = slopeHeightOffset;
            } else {
                rightHeightOffset = slopeHeightOffset;
            }
        }

        let summedHeight = y - height;
        let summedWidth = x + width;

        // bottom left
        points.push(new Point(x, y));
        // top left
        points.push(new Point(x, summedHeight - leftHeightOffset));
        // top right
        points.push(new Point(summedWidth, summedHeight - rightHeightOffset));
        // bottom right
        points.push(new Point(summedWidth, y));

        x += width;
    }

    return points;
}

function createTowerLayers(context, layers, horizon) {
    let layerBaseY = (context.canvas.height - horizon) / (Math.max(layers - 1, 1));
    let towerLayers = [];

    let colorStop1 = .1;
    let colorStop2 = .8;


    for (let i = 0; i < layers; i++) {
        let style1, style2;

        if (i === 0) {
            style1 = gradientColorPairs.back.style1;
            style2 = gradientColorPairs.back.style2;
        } else {
            style1 = gradientColorPairs.front.style1;
            style2 = gradientColorPairs.front.style2;
        }

        let gradient = createTowerGradient(context, context.canvas.width, horizon, style1, style2, colorStop1, colorStop2);
        towerLayers.push(new TowerLayer(context, horizon + layerBaseY * i, gradient));
    }

    return towerLayers;
}

let gradientColorPairs = {
    front: new StylePair(new RgbaColor(255, 0, 120), new RgbaColor(153, 52, 173)),
    back: new StylePair(new RgbaColor(100, 60, 255), new RgbaColor(0, 255, 255)),
};

let animationCanvas = document.querySelector('#animationCanvas');
let backgroundCanvas = document.querySelector('#backgroundCanvas');

let animationContext = getContext2d(animationCanvas);
let backgroundContext = getContext2d(backgroundCanvas);

let animationScene = null;
let scene = null;

let canvasAnimation = null;
let backgroundAnimation = null;

backgroundCanvas.style.backgroundColor = 'rgb(40, 40, 40)';

function init() {
    fullscreenCanvas(animationCanvas);
    fullscreenCanvas(backgroundCanvas);

    scene = new Scene(backgroundContext);
    animationScene = new AnimationScene(animationContext);

    backgroundAnimation = new CanvasAnimation(backgroundContext, scene.update.bind(scene));
    backgroundAnimation.start(false);

    canvasAnimation = new CanvasAnimation(animationContext, animationScene.update.bind(animationScene));
    canvasAnimation.start();
}

window.addEventListener('load', init);

window.addEventListener('resize', function () {
    backgroundAnimation.stop();
    canvasAnimation.stop();
    init();
});

