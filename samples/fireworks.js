class Firework extends Circle {
    constructor(context, x, y, destinationX, destinationY, radius, speed, angle, rotation, fillStyle = null, strokeStyle = null) {
        super(context, x, y, radius, angleRadiansCanvas(x, y, destinationX, destinationY), speed, angle, rotation, fillStyle, strokeStyle);
        this.destinationX = destinationX;
        this.destinationY = destinationY;
        this.trailParticles = [];
        this.effectParticles = [];
        this.particleSpawnIntervalFrames = msToFrames(30);
        this.isDetonated = false;
    }

    isAtDestination() {
        return distance(this.x, this.destinationX, this.y, this.destinationY) <= this.speed;
    }

    isActive() {
        return this.isDetonated && this.effectParticles.length === 0 && this.trailParticles.length === 0;
    }

    generateEffectParticles(particles = 15) {
        let minSpeedModifier = .3;
        let fillStyle = RgbaColor.randomColor();

        for (let i = 0; i < particles; i++) {
            let size = this.radius * randomFloatInRange(1.3, 2);
            let direction = randomFloat(TWO_PI);
            let speed = this.speed * randomFloatInRange(minSpeedModifier, minSpeedModifier + .02);

            let particle = new Particle(this.context, this.x, this.y, size, direction, speed, fillStyle);

            particle.isUseFrameDuration = true;
            particle.frameDuration = msToFrames(randomIntInRange(600, 1000, true));
            particle.fadeStartProportion = .7;

            this.effectParticles.push(particle);

        }
    }

    generateTrailParticles(particles = 1) {
        let minSpeedModifier = .2;
        let directionSpread = TWO_PI * .05;
        let fillStyle = new RgbaColor(255, 235, 170);

        for (let i = 0; i < particles; i++) {
            let size = this.radius * randomFloatInRange(.3, .7);
            let direction = this.direction + Math.PI - directionSpread / 2 + randomFloat(directionSpread);
            let speed = this.speed * randomFloatInRange(minSpeedModifier, minSpeedModifier + .02);

            let particle = new Particle(this.context, this.x, this.y, size, direction, speed, fillStyle);

            particle.isUseFrameDuration = true;
            particle.frameDuration = msToFrames(randomIntInRange(450, 650));
            particle.fadeStartProportion = .6;

            this.trailParticles.push(particle);

        }
    }

    update() {
        // destination check
        let isAtDestination = this.isAtDestination();

        if (!isAtDestination && this.frame % this.particleSpawnIntervalFrames === 0) {
            this.generateTrailParticles(randomIntInRange(4, 8, true));
        }

        if (isAtDestination && !this.isDetonated) {
            this.x = this.destinationX;
            this.y = this.destinationY;

            this.generateEffectParticles(randomIntInRange(50, 60));
            this.isDetonated = true;

            this.stop();
        }

        // update trailParticles
        for (let i = 0; i < this.trailParticles.length; i++) {
            let particle = this.trailParticles[i];
            particle.update();

            // remove particle if frame duration over or not in bounds
            if (!particle.isAlive() || !particle.isInBounds()) {
                this.trailParticles.splice(i, 1);
                i--;
            }
        }

        for (let i = 0; i < this.effectParticles.length; i++) {
            let particle = this.effectParticles[i];
            particle.update();

            // remove particle if frame duration over or not in bounds
            if (!particle.isAlive() || !particle.isInBounds()) {
                this.effectParticles.splice(i, 1);
                i--;
            }
        }

        super.update();
    }

    draw() {
        if (!this.isDetonated) {
            super.draw();
        }
    }
}

class Particle extends Square {
    constructor(context, x, y, size, direction, speed, fillStyle = null, strokeStyle = null) {
        super(context, x, y, size, direction, speed, fillStyle, strokeStyle);
        this.baseSpeed = speed;
        this.directionOffset = TWO_PI * randomFloat(.0004) * randomSign();
    }

    update() {
        this.applyAlphaFade(this.fillStyle);


        let downDirection = Math.PI + QUARTER_PI;

        if (this.direction !== downDirection) {
            // this.setDirection(Math.min((this.direction + directionOffset) % TWO_PI, downDirection));
            this.setDirection(this.direction + this.directionOffset);
        }

        this.setSpeed(Math.max(this.speed - this.baseSpeed * .012, 0));

        super.update();
    }
}

const mouse = {
    x: 0,
    y: 0
};

const canvas = document.querySelector('#animationCanvas');
const context = getContext2d(canvas);
// fullscreenCanvas(canvas);
canvas.style.backgroundColor = 'rgb(30, 30, 40)';

// let horizon;
let stars = [];
let fireworks = [];

let fireRateMs = 100;
let intervalId = 0;

function generateFireworks(numFireworks = 1, destinationX = mouse.x, destinationY = mouse.y) {
    for (let i = 0; i < numFireworks; i++) {
        let radius = canvas.height * randomFloatInRange(.004, .006);
        let x = randomIntInRange(0, canvas.width);
        let y = canvas.height + radius;
        // let destinationX = mouse.x;
        // let destinationY = mouse.y;
        let speed = canvas.height * .025;
        let fillStyle = new RgbaColor(255, 253, 247);

        fireworks.push(new Firework(context, x, y, destinationX, destinationY, radius, speed, 0, 0, fillStyle));
    }
}

function init() {
    fullscreenCanvas(canvas);

    fireworks.splice(0, fireworks.length);
    stars.splice(0, stars.length);

    for (let i = 0; i < 300; i++) {
        let size = canvas.height * randomFloatInRange(.001, .003);
        let x = randomIntInRange(size, canvas.width - size);
        let y = randomIntInRange(size, canvas.height - size);
        let fillStyle = new RgbaColor(255, 243, 227);

        stars.push(new Square(context, x, y, size, 0, 0, fillStyle));
    }

    canvasAnimation = new CanvasAnimation(context, update);
    canvasAnimation.start();
}

function update() {
    updateArray(stars);

    for (let i = 0; i < fireworks.length; i++) {
        let firework = fireworks[i];
        firework.update();

        if (firework.isActive()) {
            fireworks.splice(i, 1);
            i--;
        }
    }
}

addEventListener('load', function () {
    init();
});

addEventListener('resize', function () {
    canvasAnimation.stop();
    init();
});

addEventListener('mousemove', function (event) {
    let mousePosition = mousePositionCanvas(canvas, event);
    mouse.x = mousePosition.x;
    mouse.y = mousePosition.y;
});

canvas.addEventListener('mousedown', function () {
    generateFireworks();
    intervalId = setInterval(generateFireworks, fireRateMs);
});

canvas.addEventListener('mouseup', function () {
    clearInterval(intervalId);
});

canvas.addEventListener('mouseleave', function () {
    clearInterval(intervalId);
});