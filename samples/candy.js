class DynamicImpactCircle extends Circle {
    constructor(context, x, y, radius, direction, speed, angle, rotation, impactY, fillStyle = null, strokeStyle = null) {
        super(context, x, y, radius, direction, speed, angle, rotation, fillStyle, strokeStyle);
        this.baseSpeed = speed;
        this.impactY = impactY;
    }

    isLanded() {
        return this.y + this.radius >= this.impactY;
    }

    alphaFade() {
        let frameProportion = this.frameDurationProportion();
        let fadeStart = .8;

        if (frameProportion >= fadeStart) {
            return (1 - frameProportion) / (1 - fadeStart);
        }
        return 1;
    }

    applyAlphaFade(rgbaColor) {
        if (rgbaColor) {
            rgbaColor.alpha = this.alphaFade();
        }
    }

    update() {
        if (this.isLanded()) {
            this.isUseFrameDuration = true;
            this.stop();
        }

        this.applyAlphaFade(this.fillStyle);
        super.update();
    }
}

class BouncyCircle extends DynamicImpactCircle {
    constructor(context, x, y, radius, direction, speed, angle, rotation, impactY, fillStyle = null, strokeStyle = null) {
        super(context, x, y, radius, direction, speed, angle, rotation, impactY, fillStyle, strokeStyle);
        this.snowOverlapModifier = .3;
    }

    snowOverlap() {
        // return this.y - this.radius + this.radius * 2 * (1 - this.snowOverlapModifier);
        return this.impactY - this.radius * 2 * this.snowOverlapModifier;
    }

    isBounceX() {
        let nextX = this.nextX();
        return nextX - this.radius < 0 || nextX + this.radius >= this.context.canvas.width;
    }

    isBounceY() {
        let nextY = this.nextY();
        return nextY - this.radius < 0 || nextY + this.radius >= this.context.canvas.height;
    }

    update() {
        // bounce checks
        let isBounceX = this.isBounceX();

        // impact check
        if (this.isLanded()) {
            this.stop();
            this.rotation = 0;
        }

        // x-bounce check positive direction
        if (isBounceX && this.direction >= 0) {
            this.setDirection(Math.PI - this.direction % TWO_PI);
            // x-bounce check negative direction
        } else if (isBounceX) {
            this.setDirection(-Math.PI - this.direction % TWO_PI);
        }

        super.update();
    }
}

class Candy extends BouncyCircle {
    constructor(context, x, y, radius, direction, speed, angle, rotation, impactY) {
        super(context, x, y, radius, direction, speed, angle, rotation, impactY, colors.randomMainColor().toRgba(),
            new RgbaColor(225, 225, 225));
        this.stripes = 6 + randomInt(6, true);
        this.decaySize = radius * .002;
        this.accentStyle = colors.accent.toRgba();
        this.snowLayerLineStyle = new RgbaColor(195, 190, 190);
        this.snowParticles = [];
        this.isParticlesCreated = false;
        this.layerData = {
            x1: 0,
            y1: 0,
            cp1x: 0,
            cp1y: 0,
            x2: 0,
            y2: 0,
            cp2x: 0,
            cp2y: 0,
        };
        this.isLayerDataSet = false;
    }

    snowOverlapMiddle() {
        let snowOverlap = this.snowOverlap();
        return snowOverlap + (this.impactY - snowOverlap) / 2;
    }

    createParticles() {
        let particles = randomIntInRange(6, 10, true);

        for (let i = 0; i < particles; i++) {
            let eighthPi = QUARTER_PI / 2;
            let direction = randomFloatInRange(Math.PI - eighthPi, eighthPi);
            let speed = this.baseSpeed * randomFloatInRange(.4, .5);
            let radius = this.radius * randomFloatInRange(.05, .28);
            let x = randomIntInRange(this.x - this.radius / 2, this.x + this.radius / 2);
            let y = this.snowOverlap();
            let impactY = randomIntInRange(this.snowOverlapMiddle(), y + radius + 1);

            let snowParticle = new SnowParticle(this.context, x, y, radius, direction, speed, 0, 0, impactY);
            snowParticle.frameDuration = randomIntInRange(msToFrames(5000), msToFrames(7000), true);

            this.snowParticles.push(snowParticle);
        }
    }

    update() {
        if (this.isBounceX() || this.isBounceY()) {
            this.rotation *= 3;
            this.invertRotation();
        }

        if (this.isLayerDataSet && !this.isParticlesCreated) {
            this.createParticles();
            this.isParticlesCreated = true;
            this.isUseFrameDuration = true;
        }

        this.applyAlphaFade(this.accentStyle);
        this.applyAlphaFade(this.snowLayerLineStyle);
        this.applyAlphaFade(this.strokeStyle);

        super.update();
        updateDynamicImpactCircles(this.snowParticles);
    }

    draw() {
        let stripeAngleOffset = TWO_PI / this.stripes;
        let stripeSize = stripeAngleOffset * .25;

        super.draw();

        // draw stripes
        for (let i = 0; i < this.stripes; i++) {

            let startAngle = this.angle + stripeAngleOffset * i - stripeSize;
            let endAngle = startAngle + stripeSize * 2;

            this.context.fillStyle = getStyle(this.accentStyle);
            this.context.beginPath();
            this.context.moveTo(this.x, this.y);
            this.context.arc(this.x, this.y, this.radius, startAngle, endAngle, false);
            this.context.fill();
        }

        // draw inner circle
        drawArc(this.context, this.x, this.y, this.radius * .12, 0, TWO_PI, false, this.accentStyle);


        // candy outline
        drawArc(this.context, this.x, this.y, this.radius, 0, TWO_PI, false, null, this.strokeStyle);

        // snow layer creation
        if (this.y + this.radius > this.snowOverlap() && !this.isLanded()) {
            this.setLayerData();
            this.isLayerDataSet = true;
        }

        // draw snow layer
        if (this.isLayerDataSet) {
            this.drawSnowLayer();
        }
    }

    drawSnowLayer() {
        let bottom = this.y + this.radius + 1;

        this.context.strokeStyle = getStyle(this.snowLayerLineStyle);
        this.context.fillStyle = colors.snow;

        this.context.beginPath();
        this.context.moveTo(this.layerData.x1, this.layerData.y1);

        this.context.bezierCurveTo(this.layerData.cp1x, this.layerData.cp1y, this.layerData.cp2x, this.layerData.cp2y, this.layerData.x2, this.layerData.y2);
        this.context.stroke();

        this.context.lineTo(this.layerData.x2, bottom);
        this.context.lineTo(this.layerData.x1, bottom);
        this.context.lineTo(this.layerData.x1, this.layerData.y1);
        this.context.fill();
    }

    setLayerData() {
        let xWidthModifier = 1.2;

        this.layerData.x1 = this.x - this.radius * xWidthModifier;
        this.layerData.y1 = this.snowOverlap();
        this.layerData.cp1x = randomIntInRange(this.layerData.x1, this.x);
        this.layerData.cp1y = this.cpyRandom();

        this.layerData.x2 = this.x + this.radius * xWidthModifier;
        this.layerData.y2 = this.layerData.y1;
        this.layerData.cp2x = randomIntInRange(this.x, this.layerData.x2);
        this.layerData.cp2y = this.cpyRandom();
    }

    cpyRandom() {
        let offset = this.radius * .6;
        return randomIntInRange(this.snowOverlap() - offset, this.snowOverlap());
    }
}

class SnowParticle extends BouncyCircle {
    constructor(context, x, y, radius, direction, speed, angle, rotation, impactY) {
        super(context, x, y, radius, direction, speed, angle, rotation, impactY, colors.snowParticle.toRgba());
    }

    update() {
        // gravity if has speed
        if (this.speed !== 0) {
            this.dy += canvas.height * .00045;
        }

        super.update();
    }

    draw() {
        let size = this.radius * 2;
        drawRect(this.context, this.x - this.radius, this.y - this.radius, size, size, this.fillStyle, this.strokeStyle);
    }
}

class Snowflake extends BouncyCircle {
    constructor(context, x, y, radius, direction, speed, angle, rotation, impactY) {
        super(context, x, y, radius, direction, speed, angle, rotation, impactY, colors.snowflake.toRgba());
    }
}

class Scene {
    constructor() {
        this.candies = [];
        this.snowflakes = [];
        this.horizon = canvas.height * .45;
        this.gradients = {
            sky: this.skyGradient()
        };

        this.candyTimer = new FrameInterval(msToFrames(1500), this.addCandy.bind(this), false, 0, msToFrames(750));
        this.snowflakeTimer = new FrameInterval(msToFrames(100), this.addSnowflakes.bind(this, 10), true, msToFrames(10), msToFrames(30));
    }

    addCandy(numCandies = 1) {
        let directionRange = HALF_PI;
        let directionRangeStart = Math.PI + HALF_PI - directionRange / 2;

        for (let i = 0; i < numCandies; i++) {
            let impactY = canvas.height * randomFloatInRange(.5, .98);
            let proportion = impactY / canvas.height;
            let radius = canvas.height * .04 * proportion;
            let x = randomIntInRange(radius, canvas.width - radius);
            // let direction = randomFloatInRange(Math.PI + QUARTER_PI, Math.PI + HALF_PI + QUARTER_PI);
            let direction = directionRangeStart + randomFloat(directionRange);
            let speed = canvas.height * randomFloatInRange(.014, .015) * proportion;
            let angle = randomFloat(TWO_PI);
            let rotation = speed * .005;

            let candy = new Candy(context, x, radius, radius, direction, speed, angle, rotation, impactY);
            candy.frameDuration = msToFrames(25000);

            addInOrder(this.candies, candy);
        }
    }

    addSnowflakes(snowflakes = 1) {
        for (let i = 0; i < snowflakes; i++) {
            let impactY = canvas.height * randomFloatInRange(.5, 1);
            let proportion = impactY / canvas.height;
            let radius = canvas.height * randomFloatInRange(.003, .006) * proportion;
            let x = randomIntInRange(radius, canvas.width - radius);
            let direction = randomFloatInRange(Math.PI + QUARTER_PI, Math.PI + HALF_PI + QUARTER_PI);
            let speed = canvas.height * randomFloatInRange(.002, .005) * proportion;
            let angle = 0;
            let rotation = 0;

            let snowflake = new Snowflake(context, x, radius, radius, direction, speed, angle, rotation, impactY);
            snowflake.frameDuration = randomIntInRange(msToFrames(750), msToFrames(1500));

            addInOrder(this.snowflakes, snowflake);
        }
    }

    drawHorizon() {
        let x1 = 0;
        let y1 = canvas.height * .35;
        let x2 = canvas.width;
        let y2 = this.horizon;

        let cp1x = canvas.width * .35;
        let cp1y = canvas.height * .1;

        let cp2x = canvas.width * .65;
        let cp2y = canvas.height * .55;

        context.fillStyle = colors.snow;
        context.beginPath();
        context.moveTo(x2, y2);
        context.lineTo(x2, canvas.height);
        context.lineTo(x1, canvas.height);
        context.lineTo(x1, y1);
        context.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x2, y2);
        context.fill();
    }

    drawSky() {
        drawRect(context, 0, 0, canvas.width, canvas.height, this.gradients.sky);
    }

    skyGradient() {
        let gradient = context.createLinearGradient(0, 0, canvas.width, this.horizon);
        gradient.addColorStop(0, colors.background);
        gradient.addColorStop(1, colors.background2);
        return gradient;
    }

    update() {
        this.drawSky();
        this.drawHorizon();

        this.candyTimer.update();
        this.snowflakeTimer.update();

        updateDynamicImpactCircles(this.candies);
        updateDynamicImpactCircles(this.snowflakes);
    }
}

let canvas = document.querySelector('#animationCanvas');
let context = canvas.getContext('2d');

let scene;
let canvasAnimation;

let colors = {
    background: 'rgb(248,244,255)',
    background2: 'rgb(243, 239, 250)',
    primary: new RgbaColor(255, 40, 147),
    secondary: new RgbaColor(90, 255, 176),
    accent: new RgbaColor(247, 245, 245),
    snow: new RgbaColor(255, 250, 250),
    snowParticle: new RgbaColor(215, 215, 245),
    snowflake: new RgbaColor(235, 230, 230),
    randomMainColor() {
        let num = randomInt(2);
        if (num === 0) {
            return colors.primary;
        }
        return colors.secondary;
    }
};

function updateDynamicImpactCircles(array) {
    for (let i = 0; i < array.length; i++) {
        let circle = array[i];
        circle.update();

        if (!circle.isAlive()) {
            array.splice(i, 1);
            i--;
        }
    }
}

function addInOrder(array, scalableCircle) {
    let i = 0;
    while (i < array.length && array[i].impactY < scalableCircle.impactY) {
        i++;
    }
    array.splice(i, 0, scalableCircle);
}

function init() {
    fullscreenCanvas(canvas);
    scene = new Scene();
    canvasAnimation = new CanvasAnimation(context, scene.update.bind(scene));
    canvasAnimation.start();
}

addEventListener('load', function () {
    init();
});

addEventListener('resize', function () {
    canvasAnimation.stop();
    init();
});
