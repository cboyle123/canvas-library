# JavaScript Canvas Animation Library

![Candy Snowfall](media/image/candy_fall.gif 'Candy Snowfall')
![Fireworks](media/image/fireworks.gif 'Fireworks')
![Sunset Cityscape](media/image/sunset.gif 'Sunset Cityscape')

## Key Features
* Simplified HTML Canvas animation development.
* Extensible shape classes: easily create and animate points, rectangles, circles, and more.
* Control direction, speed, angle, and rotation of objects.
* Easily draw shapes and objects with utility drawing functions.
* Per-object shadow functionality.
* Color classes: dynamically adjust colors without string manipulation.
* Animation wrapper class: Start or stop animation loops, or draw single frames. Implement your own custom per-frame update code.
* Abstracts away tedious/repetitive drawing calls. Create animations faster and with fewer lines of code!

## Simplified Drawing
Improvements in drawing a circle:

Standard canvas code to draw a circle.
```
context.fillStyle = style;
context.beginPath();
context.moveTo(x, y);
context.arc(x, y, radius, startAngle, endAngle, anticlockwise);
context.fill();
```
Improved circle code.
```
drawArc(context, x, y, radius, startAngle, endAngle, anticlockwise, fillStyle, strokeStyle);
```
Or, an instanced circle capable of movement.
```
let circle = new Circle(context, x, y, radius, direction, speed, angle, rotation, fillStyle, strokeStyle);
circle.update();
```

## How to Use
### Getting Started
Download the contents of this repository, then include utils.js and canvas.js in your HTML file. Utils.js should be placed before canvas.js.

```
<script src="utils.js"></script>
<script src="canvas.js"></script>
```

The following example provides a basic template to start working on canvas animations.

Canvas animations are fairly straightforward to create, and only need a few things to get running:

* A ```<canvas>``` element.
* The ```<canvas>``` element's 2D context. This is what does the heavy lifting, such as performing draw calls, handling styles, etc.
* A JavaScript file to create the animation (animation.js in this example).

### Static and Dynamic Shapes
There are two ways to draw a shape: statically or dynamically. Static shapes are simply drawing calls, and do not move. 
Dynamic shapes are implemented as objects, and can be moved and manipulated.

Drawing Functions
```
drawPixel(context, x, y, fillStyle);
drawLine(context, x1, y1, x2, y2, strokeStyle);
drawArc(context, x, y, radius, startAngle, endAngle, anticlockwise, fillStyle, strokeStyle);
drawRect(context, x, y, width, height, fillStyle, strokeStyle);

// and more
```

Shape Objects
```
let pixel = new CanvasObject(context, x, y, direction, speed, angle, rotation, fillStyle, strokeStyle);
let circle = new Circle(context, x, y, radius, direction, speed, angle, rotation, fillStyle, strokeStyle);
let rectangle = new Rectangle(context, x, y, width, height, direction, speed, fillStyle, strokeStyle);

// and more
```

### Drawing Shapes
![static circle](media/image/staticCircle.png)

A basic template to draw a circle:

index.html
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Circle Demo</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>

<canvas id="animationCanvas"></canvas>

<script src="utils.js"></script>
<script src="canvas.js"></script>
<script src="animation.js"></script>

</body>
</html>
```

main.css
```
body {
    margin: 0;
    overflow: hidden;
}
```
animation.js
```
let canvas = document.querySelector('#animationCanvas');
let context = canvas.getContext('2d');

fullscreenCanvas(canvas);

let x = canvas.width / 2;
let y = canvas.height / 2;
let radius = canvas.height * .25;
let startAngle = 0;
let endAngle = TWO_PI;
let anticlockwise = false;
let fillStyle = 'blue';

drawArc(context, x, y, radius, startAngle, endAngle, anticlockwise, fillStyle);
```
### Animating the Canvas
Create live animations with the ```CanvasAnimation``` object. Pass an update function to the object to have it called each frame.

```
let canvasAnimation = new CanvasAnimation(canvas, update);
```

Start animation.
```
canvasAnimation.start();
```

Or, draw single frame only.
```
canvasAnimation.start(false);
```
Stop animation.
```
canvasAnimation.stop();
```
The following animation example continuously moves a circle left to right.

![moving circle](media/image/animatedCircleLoop.gif)

animation.js
```
let canvas = document.querySelector('#animationCanvas');
let context = canvas.getContext('2d');

fullscreenCanvas(canvas);

let x = canvas.width / 2;
let y = canvas.height / 2;
let radius = canvas.height * .25;
// 0 radians
let direction = 0;
// pixels moved per frame
let speed = canvas.width * .005;
let angle = 0;
let rotation = 0;
let fillStyle = 'lime';

let circle = new Circle(context, x, y, radius, direction, speed, angle, rotation, fillStyle);
let canvasAnimation;

// frame update function
// move circle to the right each frame
function update() {
    circle.update();

    if (!circle.isInBounds()) {
        // place circle at the left side of canvas
        circle.x = -circle.radius;
    }
}

// pass update() function to CanvasAnimation object
canvasAnimation = new CanvasAnimation(canvas, update);
canvasAnimation.start();
```
### Shadows
Use the ```Shadow``` class to quickly set shadow styles. 
```
// set the context's shadow style
Shadow.setShadow(context, shadowOffsetX, shadowOffsetY, shadowColor, shadowBlur);

// remove the context's shadow style
Shadow.removeShadow();
```

Create a ```Shadow``` object and pass it to a shape object for per-object shadow styling. This will adjust the context's shadow values when it is time to draw the object.
```
let shadow = new Shadow(context, shadowOffsetX, shadowOffsetY, shadowColor, shadowBlur);
let circle = new Circle(context, x, y, radius, direction, speed, angle, rotation, fillStyle, strokeStyle);
circle.shadow = shadow;
```
## Extending Shapes
Shapes extend from ```CanvasObject```, a class that contains most shape functionality and draws like a pixel.

Custom shapes and functionality are easily implemented. Simply extend ```CanvasObject``` (or a subclass, such as ```Rectangle```) and override functionality as needed.

Let's create a new circle class, ```BouncyCircle```, derived from ```Circle```. This shape expands upon ```Circle``` by being able to bounce off the canvas edges.

![bouncy circle](media/image/bouncyCircle.gif)

```
class BouncyCircle extends Circle {

    constructor(context, x, y, radius, direction, speed, angle, rotation, fillStyle = null, strokeStyle = null) {
        super(context, x, y, radius, direction, speed, angle, rotation, fillStyle, strokeStyle);
    }

   update() {
       let nextX = this.nextX();
       let nextY = this.nextY();

        // bounce checks
        let isBounceY = nextY - this.radius < 0 || nextY + this.radius >= this.context.canvas.height;
        let isBounceX = nextX - this.radius < 0 || nextX + this.radius >= this.context.canvas.width;

        // y-bounce check positive direction
        if (isBounceX && this.direction >= 0) {
            this.setDirection(Math.PI - this.direction % TWO_PI);
            // y-bounce check negative direction
        } else if (isBounceX) {
            this.setDirection(-Math.PI - this.direction % TWO_PI);
            // x-bounce check
        } else if (isBounceY) {
            this.setDirection(-this.direction % TWO_PI);
        }

        // Circle update call. Bounce checks are done prior to this to determine a new direction.
        super.update();
    }
}
``` 

Now, let's put everything together and create a scene with many ```BouncyCircle``` objects. The objects are added to an array and iterated over in the ```update()``` function.

![bouncy circles](media/image/bouncyCircles.gif)

bouncy.js
```
let canvas = document.querySelector('#animationCanvas');
let context = canvas.getContext('2d');

fullscreenCanvas(canvas);

let bouncyCircles = [];

// create bouncy circles
for (let i = 0; i < 30; i++) {
    // random radius size in range
    let radius = canvas.height * randomFloatInRange(.04, .05);
    let x = randomIntInRange(radius, canvas.width - radius);
    let y = randomIntInRange(radius, canvas.height - radius);
    // 0 radians
    let direction = randomFloat(TWO_PI);
    // pixels moved per frame
    let speed = canvas.height * randomFloatInRange(.005, .007);
    let angle = 0;
    let rotation = 0;
    let fillStyle = new RgbaColor(0, 0, 0);
    fillStyle.randomize();

    bouncyCircles.push(new BouncyCircle(context, x, y, radius, direction, speed, angle, rotation, fillStyle));
}

// frame update function
function update() {
    for (let i = 0; i < bouncyCircles.length; i++) {
        bouncyCircles[i].update();
    }
}

// pass update to CanvasAnimation object
let canvasAnimation = new CanvasAnimation(canvas, update);
canvasAnimation.start();
```

## Tips
* Draw multiple shape instances by adding them to an array, then iterating over it and calling ```update()``` on each object.
* A shape's ```isInBounds()``` method can typically perform different types of bounds checks.
* Style parameters accept the same type of styles that standard canvas code accepts, as well as ```RgbaColor``` objects.
* Have a mostly static scene with minor animations? Consider using a static background canvas and a separate animated canvas to increase performance.
* The ```shadowBlur``` effect is very expensive to draw. Generally avoid using this attribute in animations.
* The canvas is transparent by default. Consider a CSS background, if appropriate, instead of drawing with the canvas.

## Further Resources
* MDN
    * [Canvas Tutorial](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial)
    * [Canvas API](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API)
    * [Canvas Optimization](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Optimizing_canvas)
    * [Using window.requestAnimationFrame()](https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame)