/**
 * Animation loop class. Starts or stops an animation loop by calling the passed in update() function. Allows custom
 * per-frame update code to be run in a simple animation wrapper object.
 */
class CanvasAnimation {
    /**
     * Creates a new CanvasAnimation object.
     * @param context An HTML canvas 2d context.
     * @param update Frame update function.
     */
    constructor(context, update) {
        this.context = context;
        this.canvas = context.canvas;
        this.update = update;
        this.frameId = 0;
        this.frame = 0;
    }

    /**
     * Internal method to animate the canvas.
     * @param isLoop Run animation. If false, draw single frame only.
     */
    animate(isLoop = true) {
        clearCanvas(this.context);
        this.frameId = requestAnimationFrame(() => this.animate());
        this.update();
        this.frame++;

        if (!isLoop) {
            this.stop();
        }
    }

    /**
     * Start animation loop.
     * @param isLoop Defaults to true. If false, draws a single frame only.
     */
    start(isLoop = true) {
        this.animate(isLoop);
    }

    /**
     * Stop animation loop.
     */
    stop() {
        cancelAnimationFrame(this.frameId);
    }

    /**
     * Reset frame count to 0.
     */
    resetFrameCount() {
        this.frame = 0;
    }

    /**
     * Reset requestAnimationFrame id to 0.
     */
    resetFrameId() {
        this.frameId = 0;
    }
}

/**
 * Performs an action every frame interval. Provides a concise way to perform an action without having to manually
 * monitor passed frames. Is loosely comparable to setInterval(). Call the update() function every time a frame is run.
 */
class FrameInterval {
    /**
     * Creates a new FrameInterval object.
     * @param interval Frame interval. Action is performed on this interval.
     * @param actionCallback Callback function for desired action to be run on interval.
     * @param isStartImmediately Ignore delay and start immediately.
     * @param minDelay Minimum action delay. Delay is chosen randomly.
     * @param maxDelay Maximum action delay. Delay is chosen randomly.
     */
    constructor(interval, actionCallback, isStartImmediately = false, minDelay = 0, maxDelay = 0) {
        this.frame = 0;
        this.interval = interval;
        this.actionCallback = actionCallback;
        this.isStartImmediately = isStartImmediately;
        this.isFirstAction = true;
        this.lastActionFrame = 0;
        this.minDelay = minDelay;
        this.maxDelay = maxDelay;
        this.delay = 0;

        this.newDelay();
    }

    /**
     * Checks if the action is ready to fire.
     * @returns {boolean} Returns true if the frame interval has passed, else false.
     */
    isActionReady() {
        return this.frame > this.interval + this.delay + this.lastActionFrame || this.isStartImmediately && this.isFirstAction;
    }

    /**
     * Creates a new random delay.
     */
    newDelay() {
        this.delay = randomIntInRange(this.minDelay, this.maxDelay);
    }

    /**
     * Runs the passed in actionCallback() function.
     */
    action() {
        this.actionCallback();
        this.lastActionFrame = this.frame;
        this.newDelay();
    }

    /**
     * Update per frame. This method must be called every time a frame is updated.
     */
    update() {
        if (this.isActionReady()) {
            this.action();

            if (this.isFirstAction) {
                this.isFirstAction = false;
            }
        }
        this.frame++;
    }
}

/**
 * Applies a toggleable shadow to the passed in context.
 */
class Shadow {
    /**
     * Sets shadow properties of the passed in canvas context.
     * @param context A 2d canvas context.
     * @param shadowOffsetX x-offset.
     * @param shadowOffsetY y-offset.
     * @param shadowColor Shadow fillStyle.
     * @param shadowBlur Blur value.
     */
    static setShadow(context, shadowOffsetX = 0, shadowOffsetY = 0, shadowColor = 'transparent', shadowBlur = 0) {
        context.shadowOffsetX = shadowOffsetX;
        context.shadowOffsetY = shadowOffsetY;
        context.shadowColor = getStyle(shadowColor);
        context.shadowBlur = shadowBlur;
    }

    /**
     * Resets the shadow properties of the passed in canvas context, disabling the shadow effect.
     * @param context A 2d canvas context.
     */
    static removeShadow(context) {
        context.shadowOffsetX = 0;
        context.shadowOffsetY = 0;
        context.shadowColor = 'transparent';
        context.shadowBlur = 0;
    }

    /**
     * Creates a new shadow object.
     * @param context A 2D canvas context.
     * @param shadowOffsetX Offset the shadow on x-axis.
     * @param shadowOffsetY Offset the shadow on y-axis.
     * @param shadowColor Canvas fillStyle value or RgbaColor object.
     * @param shadowBlur Blur value. Note: can cause significant performance drops.
     * @param isActive Shadow on or off.
     */
    constructor(context, shadowOffsetX, shadowOffsetY, shadowColor, shadowBlur = 0, isActive = true) {
        this.context = context;
        this.shadowOffsetX = shadowOffsetX;
        this.shadowOffsetY = shadowOffsetY;
        this.shadowColor = shadowColor;
        this.shadowBlur = shadowBlur;
        this.isActive = isActive;
    }

    /**
     * Set the shadow for the canvas context.
     */
    activate() {
        Shadow.setShadow(this.context, this.shadowOffsetX, this.shadowOffsetY, getStyle(this.shadowColor), this.shadowBlur);
        this.isActive = true;
    }

    /**
     * Turn off the shadow for the canvas context.
     */
    deactivate() {
        Shadow.removeShadow(this.context);
        this.isActive = false;
    }
}

/**
 * Functions as a base class to be extended to draw shapes. If used by itself, class functions as a point (single
 * pixel). Provides methods to move and rotate the object, and other functionality.
 */
class CanvasObject {
    /**
     * Basic 1-pixel shape.
     * @param context A 2D canvas context.
     * @param x X value.
     * @param y Y value.
     * @param direction Movement angle in radians.
     * @param speed Pixels to move per update.
     * @param angle Rotation angle in radians.
     * @param rotation Rotation per update in radians.
     * @param fillStyle Canvas fillStyle value or RgbaColor object.
     * @param strokeStyle Canvas style value or RgbaColor object.
     */
    constructor(context, x, y, direction, speed, angle, rotation, fillStyle = null, strokeStyle = null) {
        this.context = context;
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.speed = speed;
        this.angle = angle;
        this.rotation = rotation % TWO_PI;
        this.dx = 0;
        this.dy = 0;
        this.fillStyle = fillStyle;
        this.strokeStyle = strokeStyle;
        this.shadow = null;
        this.frame = 0;
        this.isUseFrameDuration = false;
        this.prevIsUseFrameDuration = this.isUseFrameDuration;
        this.frameDurationStart = 0;
        this.frameDuration = 0;
        this.fadeStartProportion = .8;

        this.updateDxDy();
    }

    /**
     * Sets direction value.
     * @param direction A direction angle in radians.
     */
    setDirection(direction) {
        this.direction = direction % TWO_PI;
        this.updateDxDy();
    }

    /**
     * Sets speed value.
     * @param speed A speed value in pixels.
     */
    setSpeed(speed) {
        this.speed = speed;
        this.updateDxDy();
    }

    /**
     * Updates dx and dy values based on current direction and speed.
     */
    updateDxDy() {
        let moveData = calculateDxDyCanvas(this.direction, this.speed);
        this.dx = moveData.dx;
        this.dy = moveData.dy;
    }

    /**
     * X-value next update (x + dx).
     * @returns {*} Returns x-value for next update.
     */
    nextX() {
        return this.x + this.dx;
    }

    /**
     * Y-value next update (y + dy).
     * @returns {*} Returns y-value for next update.
     */
    nextY() {
        return this.y + this.dy;
    }

    /**
     * Inverts the current rotation direction. (e.g. clockwise would become counterclockwise).
     */
    invertRotation() {
        this.rotation *= -1;
    }

    /**
     * Moves object by updating x and y values by adding respective dx and dy values.
     */
    move() {
        this.x += this.dx;
        this.y += this.dy;
    }

    /**
     * Set movement speed to 0.
     */
    stop() {
        this.setSpeed(0);
    }

    /**
     * Turns the object's shadow effect on or off.
     * @param isActive If true, shadow effects will be drawn, assuming a Shadow object is assigned to the object.
     */
    setShadowActive(isActive) {
        if (this.shadow && isActive) {
            this.shadow.activate();
        } else if (this.shadow && !isActive) {
            this.shadow.deactivate();
        }
    }

    /**
     * Draws the object on the canvas.
     */
    draw() {
        this.setShadowActive(true);
        drawPixel(this.context, this.x, this.y, this.fillStyle);
        this.setShadowActive(false);
    }

    /**
     * Updates the current angle based on current rotation value or passed in value.
     * @param angle A new angle value.
     */
    updateAngle(angle = null) {
        if (angle || angle === 0) {
            this.angle = angle % TWO_PI;
        } else {
            this.angle = (this.angle + this.rotation) % TWO_PI;
        }
    }

    /**
     * Moves and draws the object.
     */
    update() {
        this.updateAngle();
        this.move();
        this.handleFrameDuration();
        this.draw();
    }

    /**
     * Checks if the object is in bounds of the passed in canvas. Use CanvasObject.bounds values to change bounds check.
     * @param boundsCheck Type of bounds check to perform.
     * @returns {boolean} Returns true if in canvas bounds, else false.
     */
    isInBounds(boundsCheck = CanvasObject.bounds.whole) {
        return isInBounds(this.context.canvas, this.x, this.y);
    }

    /**
     * Set frame count to specified value (0 if not specified).
     */
    resetFrame(frame = 0) {
        this.frame = frame;
    }

    /**
     * Increments current frame value by 1.
     */
    incrementFrame() {
        this.frame++;
    }

    /**
     * Handles frame duration checks and updates performed each frame.
     */
    handleFrameDuration() {
        if (this.isUseFrameDuration !== this.prevIsUseFrameDuration) {
            this.prevIsUseFrameDuration = this.isUseFrameDuration;

            if (this.isUseFrameDuration) {
                this.frameDurationStart = this.frame;
            }
        }

        this.incrementFrame();
    }

    /**
     * Calculates the proportion of passed frames to total duration frames in the frame duration lifetime.
     * @returns {number} Returns the proportion as a decimal. If frame duration is not in use, returns 0.
     */
    frameDurationProportion() {
        if (this.isUseFrameDuration) {
            return this.frame / (this.frameDurationStart + this.frameDuration);
        }

        return 0;
    }

    /**
     * Checks if the frame duration has expired. Use this method as a flag to determine when to remove expired objects.
     * @returns {boolean} Returns true if frame duration has not be reached. Always returns if frame duration is not in
     *     use.
     */
    isAlive() {
        if (this.isUseFrameDuration) {
            return this.frame < this.frameDurationStart + this.frameDuration;
        }

        return true;
    }

    /**
     * Calculates fade for frame duration end effect.
     * @returns {number} Returns fade value. Returns 1 if frame duration not in use.
     */
    alphaFade() {
        let frameProportion = this.frameDurationProportion();

        if (this.isUseFrameDuration && frameProportion >= this.fadeStartProportion) {
            return (1 - frameProportion) / (1 - this.fadeStartProportion);
        }
        return 1;
    }

    /**
     * Applies alpha fade value for frame duration to passed in color value.
     * @param style RgbaColor or HslaColor object.
     */
    applyAlphaFade(style) {
        if (style instanceof RgbaColor || style instanceof HslaColor) {
            style.alpha = this.alphaFade();
        }
    }
}

/**
 * Types of boundary checks.
 * @type {{edge: number, whole: number}}
 */
CanvasObject.bounds = {
    /**
     * Edge bounds check (shape is out of bounds if edge out of bounds).
     */
    edge: 0,
    /**
     * Whole bounds check (whole shape must be out of bounds).
     */
    whole: 1
};

/**
 * An Object for drawing circles on the canvas.
 */
class Circle extends CanvasObject {
    /**
     * Creates a new Circle object.
     * @param context A 2D canvas context
     * @param x X value.
     * @param y Y value.
     * @param radius Circle radius in pixels.
     * @param direction Movement angle in radians.
     * @param speed Pixels to move per update.
     * @param angle Rotation angle in radians.
     * @param rotation Rotation per update in radians.
     * @param fillStyle Canvas fillStyle value or RgbaColor object.
     * @param strokeStyle Canvas style value or RgbaColor object.
     */
    constructor(context, x, y, radius, direction, speed, angle, rotation, fillStyle = null, strokeStyle = null) {
        super(context, x, y, direction, speed, angle, rotation, fillStyle, strokeStyle);
        this.radius = radius;
    }

    /**
     * Draws the circle.
     */
    draw() {
        this.setShadowActive(true);
        drawCircle(this.context, this.x, this.y, this.radius, this.fillStyle, this.strokeStyle);
        this.setShadowActive(false);
    }

    /**
     * Gets a coordinate point from the circle's center point based on the chosen and distance from the center.
     * Chosen distance can exceed the radius of the circle.
     * @param angle The angle in radians from the circle's center point.
     * @param distance The pixel distance from the circle's center point.
     * @returns {Point} Returns a Point for the chosen point.
     */
    getPoint(angle = 0, distance = this.radius) {
        return circlePoint(this.x, this.y, angle, distance);
    }

    /**
     * Checks if the object is in bounds of the passed in canvas. Use CanvasObject.bounds values to change bounds check.
     * @param boundsCheck Type of bounds check to perform.
     * @returns {boolean} Returns true if in canvas bounds, else false.
     */
    isInBounds(boundsCheck = CanvasObject.bounds.whole) {
        let canvas = this.context.canvas;
        if (boundsCheck === CanvasObject.bounds.whole) {
            return this.x + this.radius >= 0 && this.x - this.radius < canvas.width
                && this.y + this.radius >= 0 && this.y - this.radius < canvas.height;
        } else {
            return this.x - this.radius >= 0 && this.x + this.radius < canvas.width
                && this.y - this.radius >= 0 && this.y + this.radius < canvas.height;
        }
    }
}

/**
 * An object for drawing rectangles on the canvas.
 */
class Rectangle extends CanvasObject {
    /**
     * Creates a new Rectangle object.
     * @param context A 2D canvas context.
     * @param x X value.
     * @param y Y value.
     * @param width Width in pixels.
     * @param height Height in pixels.
     * @param direction Movement angle in radians.
     * @param speed Pixels to move per update.
     * @param fillStyle Canvas fillStyle value or RgbaColor object.
     * @param strokeStyle Canvas style value or RgbaColor object.
     */
    constructor(context, x, y, width, height, direction, speed, fillStyle = null, strokeStyle = null) {
        super(context, x, y, direction, speed, 0, 0, fillStyle, strokeStyle);
        this.width = width;
        this.height = height;
    }

    /**
     * Draws the rectangle.
     */
    draw() {
        this.setShadowActive(true);
        drawRect(this.context, this.x, this.y, this.width, this.height, this.fillStyle, this.strokeStyle);
        this.setShadowActive(false);
    }

    /**
     * Middle x value.
     * @returns {*} Returns the middle x value.
     */
    getCenterX() {
        return this.x + this.width / 2;
    }

    /**
     * Middle y value.
     * @returns {*} Returns the middle y value.
     */
    getCenterY() {
        return this.y + this.height / 2;
    }

    /**
     * Checks if the object is in bounds of the passed in canvas. Use CanvasObject.bounds values to change bounds check.
     * @param boundsCheck Type of bounds check to perform.
     * @returns {boolean} Returns true if in canvas bounds, else false.
     */
    isInBounds(boundsCheck = CanvasObject.bounds.whole) {
        let canvas = this.context.canvas;
        if (boundsCheck === CanvasObject.bounds.whole) {
            return this.x + this.width >= 0 && this.x < canvas.width && this.y + this.height >= 0
                && this.y < canvas.height;
        } else {
            return this.x >= 0 && this.x + this.width < canvas.width && this.y >= 0
                && this.y + this.height < canvas.height;
        }
    }
}

/**
 * Square object extending Rectangle.
 */
class Square extends Rectangle {
    /**
     * Creates a new Rectangle object.
     * @param context A 2D canvas context.
     * @param x X value.
     * @param y Y value.
     * @param size Size in pixels.
     * @param direction Movement angle in radians.
     * @param speed Pixels to move per update.
     * @param fillStyle Canvas fillStyle value or RgbaColor object.
     * @param strokeStyle Canvas style value or RgbaColor object.
     */
    constructor(context, x, y, size, direction, speed, fillStyle = null, strokeStyle = null) {
        super(context, x, y, size, size, direction, speed, fillStyle, strokeStyle);
    }
}

/**
 * An equilateral triangle.
 */
class Triangle extends CanvasObject {
    /**
     * Creates a new Triangle object.
     * @param context A 2D canvas context.
     * @param x X value.
     * @param y Y value.
     * @param size Distance from center to triangle corner.
     * @param direction Movement angle in radians.
     * @param speed Pixels to move per update.
     * @param angle Rotation angle in radians.
     * @param rotation Rotation per update in radians.
     * @param fillStyle Canvas fillStyle value or RgbaColor object.
     * @param strokeStyle Canvas style value or RgbaColor object.
     */
    constructor(context, x, y, size, direction, speed, angle, rotation, fillStyle = null, strokeStyle = null) {
        super(context, x, y, direction, speed, angle, rotation, fillStyle, strokeStyle);
        this.size = size;
    }

    /**
     * Get the three corner points of the triangle.
     * @returns {Array} Returns an array of Point objects.
     */
    getCornerPoints() {
        let centerOffsets = [];
        let cornerPoints = [];

        for (let i = 0; i < 3; i++) {
            centerOffsets.push(calculateDxDyCanvas(TWO_PI * .3333 * i + this.angle, this.size));
        }

        for (let i = 0; i < centerOffsets.length; i++) {
            let offset = centerOffsets[i];
            cornerPoints.push(new Point(this.x + offset.dx, this.y + offset.dy));
        }

        return cornerPoints;
    }

    /**
     * Draws the triangle.
     */
    draw() {
        this.setShadowActive(true);

        let cornerPoints = this.getCornerPoints();
        drawTriangle(this.context, cornerPoints[0].x, cornerPoints[0].y, cornerPoints[1].x, cornerPoints[1].y, cornerPoints[2].x, cornerPoints[2].y, this.fillStyle, this.strokeStyle);

        this.setShadowActive(false);
    }

    /**
     * Checks if the object is in bounds of the passed in canvas. Use CanvasObject.bounds values to change bounds check.
     * @param boundsCheck Type of bounds check to perform.
     * @returns {boolean} Returns true if in canvas bounds, else false.
     */
    isInBounds(boundsCheck = CanvasObject.bounds.whole) {
        let cornerPoints = this.getCornerPoints();
        let outOfBoundsCount = 0;

        for (let i = 0; i < cornerPoints.length; i++) {
            let isCornerInBounds = isInBoundsPoint(this.context.canvas, cornerPoints[i]);

            if (boundsCheck === CanvasObject.bounds.edge && !isCornerInBounds) {
                return false;
            }

            if (boundsCheck === CanvasObject.bounds.whole && !isCornerInBounds) {
                outOfBoundsCount++;
            }
        }

        if (boundsCheck === CanvasObject.bounds.whole) {
            return outOfBoundsCount < 3;
        }

        return true;
    }
}

/**
 * Utility function to set strokeStyle and fillStyle to the passed in parameter values.
 * @param context A 2D canvas context.
 * @param fillStyle A fillStyle String or RgbaColor value.
 * @param strokeStyle A fillStyle String or RgbaColor value.
 */
function prepare(context, fillStyle = null, strokeStyle = null) {
    if (strokeStyle) {
        context.strokeStyle = getStyle(strokeStyle);
    }

    if (fillStyle) {
        context.fillStyle = getStyle(fillStyle);
    }
    context.beginPath();
}

/**
 * Finalizes a canvas path by calling fill(), stroke(), and closePath() as specified in parameters.
 * @param context A 2D canvas context.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 * @param isClosed Close the path.
 */
function finalize(context, fillStyle, strokeStyle, isClosed = false) {
    if (strokeStyle && isClosed) {
        context.closePath();
        context.stroke();
    } else if (strokeStyle) {
        context.stroke();
    }

    if (fillStyle) {
        context.fill();
    }
}

/**
 * Handle common drawing operations before and after executing passed in drawing function.
 * @param context A 2d canvas context.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 * @param drawingCallback Custom drawing function.
 * @param isClosed Call closePath().
 * @param isUseFinalize Call finalize() after drawingCallback().
 * @param isUsePrepare Call prepare() before drawingCallback().
 */
function handleDrawing(context, fillStyle, strokeStyle, drawingCallback, isClosed = false, isUseFinalize = true, isUsePrepare = true) {
    if (isFunction(drawingCallback)) {

        if (isUsePrepare) {
            prepare(context, fillStyle, strokeStyle);
        }

        drawingCallback();

        if (isUseFinalize) {
            finalize(context, fillStyle, strokeStyle, isClosed);
        }
    }
}

/**
 * Draws a single pixel point.
 * @param context A 2D canvas context.
 * @param x X-value.
 * @param y Y-value.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 */
function drawPixel(context, x, y, fillStyle) {
    let size = 1;

    handleDrawing(context, fillStyle, null, () => {
        context.moveTo(x, y);

        if (fillStyle) {
            context.fillRect(x, y, size, size);
        }
    }, false, false);
}

/**
 * Draws a line.
 * @param context A 2D canvas context.
 * @param x1 Starting x value.
 * @param y1 Starting y value.
 * @param x2 Ending x value.
 * @param y2 Ending y value.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 */
function drawLine(context, x1, y1, x2, y2, strokeStyle) {
    handleDrawing(context, null, strokeStyle, () => {
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
    });
}

/**
 * Draws a filled or unfilled rectangle.
 * @param context A 2D canvas context.
 * @param x X value.
 * @param y Y value.
 * @param width Rectangle width in pixels.
 * @param height Rectangle height in pixels.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 */
function drawRect(context, x, y, width, height, fillStyle = null, strokeStyle = null) {
    handleDrawing(context, fillStyle, strokeStyle, () => {
        if (fillStyle) {
            context.fillRect(x, y, width, height);
        }

        if (strokeStyle) {
            context.strokeRect(x, y, width, height);
        }
    }, false, false);
}

/**
 * Draws a filled or unfilled triangle.
 * @param context A 2D canvas context.
 * @param x1 Corner x value.
 * @param y1 Corner y value.
 * @param x2 Corner x value.
 * @param y2 Corner y value.
 * @param x3 Corner x value.
 * @param y3 Corner y value.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 */
function drawTriangle(context, x1, y1, x2, y2, x3, y3, fillStyle = null, strokeStyle = null) {
    handleDrawing(context, fillStyle, strokeStyle, () => {
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.lineTo(x3, y3);
    }, true);
}

/**
 * Draws a filled or unfilled arc.
 * @param context A 2D canvas context.
 * @param x Center point x value.
 * @param y Center point y value.
 * @param radius Radius in pixels.
 * @param startAngle Starting angle in radians.
 * @param endAngle Ending angle in radians.
 * @param anticlockwise Draw arc anticlockwise.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 * @param isClosed Close the arc (connect starting and ending points with a line).
 */
function drawArc(context, x, y, radius, startAngle, endAngle, anticlockwise = false, fillStyle = null, strokeStyle = null, isClosed = false) {
    handleDrawing(context, fillStyle, strokeStyle, () => {
        context.arc(x, y, radius, startAngle, endAngle, anticlockwise);
    }, isClosed);
}

/**
 * Draws a filled or unfilled arc.
 * @param context A 2D canvas context.
 * @param x Center point x value.
 * @param y Center point y value.
 * @param radius Radius in pixels.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 */
function drawCircle(context, x, y, radius, fillStyle = null, strokeStyle = null) {
    handleDrawing(context, fillStyle, strokeStyle, () => {
        drawArc(context, x, y, radius, 0, TWO_PI, false, fillStyle, strokeStyle);
    });
}

function drawQuadraticCurve(context, x1, y1, x2, y2, cpx, cpy, fillStyle = null, strokeStyle = null, isClosed = false) {
    handleDrawing(context, fillStyle, strokeStyle, () => {
        context.moveTo(x1, y1);
        context.quadraticCurveTo(cpx, cpy, x2, y2);
    }, isClosed);
}

/**
 * Draws a cubic bezier curve.
 * @param context A 2D canvas context.
 * @param x1 Starting x value.
 * @param y1 Starting y value.
 * @param x2 Ending x value.
 * @param y2 Ending y value.
 * @param cp1x Control point 1 x value.
 * @param cp1y Control point 1 y value.
 * @param cp2x Control point 2 x value.
 * @param cp2y Control point 2 y value.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 * @param isClosed Close the line.
 */
function drawCubicBezierCurve(context, x1, y1, x2, y2, cp1x, cp1y, cp2x, cp2y, fillStyle = null, strokeStyle = null, isClosed = false) {
    handleDrawing(context, fillStyle, strokeStyle, () => {
        context.moveTo(x1, y1);
        context.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x2, y2);
    }, isClosed)
}

/**
 * Draws a custom shape based on passed in array of coordinates.
 * @param context A 2D canvas context.
 * @param points An array of points to connect lines.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 * @param isClosed Close the line.
 */
function drawPolygon(context, points, fillStyle = null, strokeStyle = null, isClosed = false) {
    if (points.length > 0) {
        let point = points[0];

        prepare(context, fillStyle, strokeStyle);

        context.moveTo(point.x, point.y);

        for (let i = 1; i < points.length; i++) {
            point = points[i];
            context.lineTo(point.x, point.y);
        }

        finalize(context, fillStyle, strokeStyle, isClosed);
    }
}

/**
 * Draws text at specified point.
 * @param context A 2D canvas context.
 * @param x X-value.
 * @param y Y-value.
 * @param fontSize Font size in pixels.
 * @param font Font string.
 * @param text Text to be drawn.
 * @param fillStyle Canvas fillStyle value or RgbaColor object.
 * @param strokeStyle Canvas fillStyle value or RgbaColor object.
 */
function drawText(context, x, y, fontSize, font, text, fillStyle = null, strokeStyle = null) {
    context.font = fontSize.toString() + 'px ' + font;

    handleDrawing(context, fillStyle, strokeStyle, () => {
        if (fillStyle) {
            context.fillText(text, x, y);
        }

        if (strokeStyle) {
            context.strokeText(text, x, y);
        }
    }, false, false);
}

/**
 * Sets the width and height of the passed in canvas.
 * @param canvas A canvas element.
 * @param width Width in pixels.
 * @param height Height in pixels.
 */
function setCanvasSize(canvas, width, height) {
    canvas.width = width;
    canvas.height = height;
}

/**
 * Sets the canvas element to fullscreen size.
 * @param canvas A canvas element.
 */
function fullscreenCanvas(canvas) {
    setCanvasSize(canvas, innerWidth, innerHeight);
}

/**
 * Gets the center x-value of the canvas.
 * @param canvas A canvas element.
 * @returns {number} Returns center x-value.
 */
function canvasCenterX(canvas) {
    return canvas.width / 2;
}

/**
 * Gets the center y-value of the canvas.
 * @param canvas A canvas element.
 * @returns {number} Returns center y-value.
 */
function canvasCenterY(canvas) {
    return canvas.height / 2;
}

/**
 * Gets the center x-value and y-value of the canvas as a point.
 * @param canvas A canvas element.
 * @returns {Point} Returns a Point object.
 */
function canvasCenterPoint(canvas) {
    return new Point(canvasCenterX(canvas), canvasCenterY(canvas));
}

/**
 * Calls clearRect() on the passed in canvas context.
 * @param context A 2d canvas context.
 * @param x Starting x-value.
 * @param y Starting y-value.
 * @param width Width to clear.
 * @param height Height to clear.
 */
function clearCanvas(context, x = 0, y = 0, width = context.canvas.width, height = context.canvas.height) {
    context.clearRect(x, y, width, height);
}

/**
 * Sets the background color of a canvas element.
 * @param canvas A canvas element.
 * @param background A color string or RgbaColor object.
 */
function setCanvasBackgroundColor(canvas, background) {
    canvas.style.backgroundColor = getStyle(background);
}

/**
 * Utility function to handle fillStyle values. Use when assigning a value to a canvas context's fillStyle attribute.
 * Allows RgbaColor objects to be used instead of fillStyle strings, while preserving default canvas fillStyle
 * functionality.
 * @param style A fillStyle string, RgbaColor object, or a fillStyle usable by a canvas context.
 * @returns {string|*} If the fillStyle is an instance of RgbaColor, returns its fillStyle string, else simply returns
 *     the passed in fillStyle.
 */
function getStyle(style) {
    if (style instanceof RgbaColor || style instanceof HslaColor) {
        return style.toString();
    }
    return style;
}

/**
 * Gets the 2D context of a canvas.
 * @param canvas A canvas element.
 * @returns {CanvasRenderingContext2D | WebGLRenderingContext}
 */
function getContext2d(canvas) {
    return canvas.getContext('2d');
}

/**
 * Check if x-value is in canvas bounds.
 * @param canvas Canvas element.
 * @param x X-value.
 * @returns {boolean} Returns true if x-value is in canvas bounds, else false.
 */
function isInBoundsX(canvas, x) {
    return x >= 0 && x < canvas.width;
}


/**
 * Check if y-value is in canvas bounds.
 * @param canvas Canvas element.
 * @param y Y-value.
 * @returns {boolean} Returns true if y-value is in canvas bounds, else false.
 */
function isInBoundsY(canvas, y) {
    return y >= 0 && y < canvas.height;
}

/**
 * Check if x-value and y-value are in canvas bounds.
 * @param canvas Canvas element.
 * @param x X-value.
 * @param y Y-value.
 * @returns {boolean} Returns true if x-value and y-value are in canvas bounds, else false.
 */
function isInBounds(canvas, x, y) {
    return isInBoundsX(canvas, x) && isInBoundsY(canvas, y);
}

/**
 * Check if x-value and y-value (Point object) are in canvas bounds.
 * @param canvas Canvas element.
 * @param point A Point object.
 * @returns {boolean} Returns true if point's x-value and y-value are in canvas bounds, else false.
 */
function isInBoundsPoint(canvas, point) {
    return isInBounds(canvas, point.x, point.y);
}

/**
 * Calls update() function for each object in array.
 * @param array An array of CanvasObjects.
 */
function updateArray(array) {
    for (let i = 0; i < array.length; i++) {
        array[i].update();
    }
}