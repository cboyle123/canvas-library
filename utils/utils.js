const TWO_PI = 2 * Math.PI;
const HALF_PI = Math.PI / 2;
const QUARTER_PI = Math.PI / 4;

const MAX_DEGREES = 360;
const HALF_DEGREES = MAX_DEGREES / 2;
const QUARTER_DEGREES = MAX_DEGREES / 4;

const RGB_MAX_VALUE = 255;
const HSL_MAX_HUE = 360;
const HSL_MAX_SATURATION = 100;
const HSL_MAX_LIGHTNESS = 100;

/**
 * A two-dimensional x and y coordinate point.
 */
class Point {
    /**
     * Sets x and y values.
     * @param x X value.
     * @param y Y value.
     */
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Calculate the distance between this point and the passed in point
     * @param point a Point object
     */
    distance(point) {
        distance(this.x, point.x, this.y, point.y)
    }

    /**
     * String representation.
     * @returns {string} Formatted string
     */
    toString() {
        return this.x.toString() + ', ' + this.y.toString();
    }
}

/**
 * Wrapper class to hold data for a priority queue.
 */
class PriorityQueueNode {
    /**
     * Takes an item, item's priority value, and index
     * @param item The value to be stored
     * @param priority The item's priority value (affects queue placement)
     * @param index Index to store the node's place in priority queue heap array.
     */
    constructor(item, priority, index) {
        this.item = item;
        this.priority = priority;
        this.index = index;
    }

    /**
     * String representation.
     * @returns {string} A formatted string.
     */
    toString() {
        return this.item.toString();
    }
}

/**
 * A priority-based queue. Requires a callback function to determine priority of contents.
 */
class PriorityQueue {
    /**
     * Creates a PriorityQueue object with the passed in callback used to manage ordering of contents.
     * @param priorityCallback A callback function that takes a single parameter. Must return a numerical value,
     *     which will then be used to determine an item's priority. This function will be called when an item is
     *     inserted to determine it's priority value.
     */
    constructor(priorityCallback) {
        this.heap = [null];
        this.priorityCallback = priorityCallback;
    }

    /**
     * Inserts an item into the priority queue.
     * @param item An Object.
     */
    insert(item) {
        let index = this.size() + 1;
        let node = new PriorityQueueNode(item, this.priorityCallback(item), index);
        let parent = this.parent(node);
        this.heap.push(node);

        while (parent !== null && node.priority < parent.priority) {

            // swap node and parent
            this.swapNodes(node, parent);

            // update parent
            parent = this.parent(node);
        }
    }

    /**
     * Removes and returns the highest-priority item in the queue.
     * @returns {*} A queue item.
     */
    remove() {
        if (!this.isEmpty()) {
            let root = this.peek();
            let node = this.heap[this.heap.length - 1];
            this.heap.pop();

            this.heap[root.index] = node;
            node.index = root.index;

            let leftChild = this.leftChild(node);
            let rightChild = this.rightChild(node);

            while ((this.hasLeftChild(node) && node.priority > leftChild.priority) || (this.hasRightChild(node)
                && node.priority > rightChild.priority)) {
                let children = this.children(node);
                let swapNode;

                if (children.length === 1) {
                    swapNode = children[0];
                } else if (leftChild.priority < rightChild.priority) {
                    swapNode = leftChild;
                } else {
                    swapNode = rightChild;
                }
                this.swapNodes(node, swapNode);

                leftChild = this.leftChild(node);
                rightChild = this.rightChild(node);
            }
            return root.item;
        }
    }

    /**
     * Returns, but does not remove, the highest priority item in the queue.
     * @returns {*} A queue item.
     */
    peek() {
        if (!this.isEmpty()) {
            return this.root();
        }
    }

    /**
     * Checks if the passed in item exists in the queue. Uses
     * @param item A value to search for.
     * @param isEqualCallback An equality function that takes two objects as parameters.
     * @returns {boolean} Returns true if the item exists in the queue, else false.
     */
    contains(item, isEqualCallback = null) {
        for (let i = 1; i < this.heap.length; i++) {
            if (isEqual(this.heap[i].item, item, isEqualCallback)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the queue is empty.
     * @returns {boolean} Returns true for an empty queue, else false.
     */
    isEmpty() {
        return this.size() === 0;
    }


    /**
     * The number of items in the queue.
     * @returns {number} Returns item count.
     */
    size() {
        return this.heap.length - 1;
    }

    /**
     * Gets the root node.
     * @returns {*} Returns the root node.
     */
    root() {
        return this.heap[1];
    }

    /**
     * Gets the parent node of the passed in node.
     * @param node A PriorityQueueNode object.
     * @returns {*} Returns the parent PriorityQueueNode.
     */
    parent(node) {
        return this.heap[Math.floor(node.index / 2)];
    }

    /**
     * Gets child nodes of the passed in node.
     * @param node A PriorityQueueNode object.
     * @returns {Array} Returns an array of PriorityQueueNodes.
     */
    children(node) {
        let children = [];

        if (this.hasLeftChild(node)) {
            children.push(this.leftChild(node));
        }

        if (this.hasRightChild(node)) {
            children.push(this.rightChild(node));
        }

        return children;
    }

    /**
     * Get left child node.
     * @param node A PriorityQueueNode object.
     * @returns {Array} Returns a PriorityQueueNode.
     */
    leftChild(node) {
        let index = node.index * 2;
        if (index < this.heap.length) {
            return this.heap[index];
        }

        return null;
    }

    /**
     * Get right child node.
     * @param node A PriorityQueueNode object.
     * @returns {Array} Returns a PriorityQueueNode.
     */
    rightChild(node) {
        let index = node.index * 2 + 1;
        if (index < this.heap.length) {
            return this.heap[index];
        }

        return null;
    }

    /**
     * Checks if left child node exists.
     * @param node A PriorityQueueNode object.
     * @returns {boolean} Returns true if left child node exists, else false.
     */
    hasLeftChild(node) {
        return this.leftChild(node) !== null;
    }

    /**
     * Checks if the right child node exists.
     * @param node A PriorityQueueNode object.
     * @returns {boolean} Returns true if right child node exists, else false.
     */
    hasRightChild(node) {
        return this.rightChild(node) !== null;
    }

    /**
     * Checks if any child nodes exist for the passed in node.
     * @param node A PriorityQueueNode object.
     * @returns {boolean} Returns true if any child node exists for the passed in node, else false.
     */
    hasChildren(node) {
        return this.hasLeftChild(node) || this.hasRightChild(node);
    }

    /**
     * Utility function to swap nodes.
     * @param node1 A PriorityQueueNode object.
     * @param node2 A PriorityQueueNode object.
     */
    swapNodes(node1, node2) {
        let temp = node1;
        let tempIndex = node1.index;

        this.heap[node1.index] = node2;
        this.heap[node2.index] = temp;

        node1.index = node2.index;
        node2.index = tempIndex;
    }

    /**
     * Creates a string representation of the queue.
     * @returns {string} Returns the formatted string.
     */
    toString() {
        return this.heap.toString();
    }
}

/**
 * Allows color values to be dynamically adjusted instead of relying on the creation or modification of
 * color strings. Uses RGBA format.
 *
 * Stores red, green, blue, and alpha values. Alpha is optional and assigned to 1 by default. To use the color
 * value, call the toString() method, which returns a formatted RGBA color string.
 *
 */
class RgbaColor {
    /**
     * Generates an RgbaColor object with random color values.
     * @param alpha Optional alpha value. Defaults to 1.
     * @returns {RgbaColor} Returns the randomly generated RgbaColor object.
     */
    static randomColor(alpha = 1) {
        let rgbaColor = new RgbaColor(0, 0, 0, alpha);
        rgbaColor.randomize();
        return rgbaColor;
    }

    /**
     * Creates an RgbaColor object with given color values. Alpha is optional and defaults to 1.
     *
     * @param red Red value.
     * @param green Green value.
     * @param blue Blue value.
     * @param alpha Alpha (transparency) value.
     */
    constructor(red, green, blue, alpha = 1) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    /**
     * Randomizes the current color values. Does not change alpha value.
     */
    randomize() {
        const max = 255;
        this.red = randomInt(max, true);
        this.green = randomInt(max, true);
        this.blue = randomInt(max, true);
    }

    /**
     * Convert to Hsla.
     * @returns {HslaColor} An HslaColor object with equivalent color and alpha values.
     */
    toHsla() {
        let degrees = 60;

        let red = this.red / RGB_MAX_VALUE;
        let green = this.green / RGB_MAX_VALUE;
        let blue = this.blue / RGB_MAX_VALUE;

        let cMax = Math.max(red, green, blue);
        let cMin = Math.min(red, green, blue);
        let delta = cMax - cMin;

        let hue, saturation, lightness;

        // hue calculation
        if (delta === 0) {
            hue = 0;
        } else if (cMax === red) {
            hue = degrees * ((green - blue) / delta % 6);
        } else if (cMax === green) {
            hue = degrees * ((blue - red) / delta + 2);
        } else if (cMax === blue) {
            hue = degrees * ((red - green) / delta + 4);
        }

        // lightness calculation
        lightness = (cMax + cMin) / 2;

        // saturation calculation
        if (delta === 0) {
            saturation = 0;
        } else {
            saturation = delta / (1 - Math.abs(2 * lightness - 1));
        }

        hue = Math.round(hue);
        saturation = Math.round(saturation * 100);
        lightness = Math.round(lightness * 100);

        if (hue > HSL_MAX_HUE) {
            hue = HSL_MAX_HUE;
        }

        if (saturation > HSL_MAX_SATURATION) {
            saturation = HSL_MAX_SATURATION;
        }

        if (lightness > HSL_MAX_LIGHTNESS) {
            lightness = HSL_MAX_LIGHTNESS;
        }

        return new HslaColor(hue, saturation, lightness, this.alpha);
    }

    /**
     * Creates a new RgbaColor object with the same values as the calling object.
     * @returns {RgbaColor} A new RbgaColor object with same values.
     */
    toRgba() {
        return new RgbaColor(this.red, this.green, this.blue, this.alpha);
    }

    /**
     * Returns a string matching standard RGBA syntax for CSS, HTML, etc. E.g. with 0, 255, 100, .8, returns
     * 'rgba(0, 255, 100, .8)'.
     * @returns {string} Returns a formatted RGBA string.
     */
    toString() {
        return 'rgba(' + this.red + ', ' + this.green + ', ' + this.blue + ', ' + this.alpha + ')';
    }
}

/**
 * Allows color values to be dynamically adjusted instead of relying on the creation or modification of
 * color strings. Uses HSLA format.
 *
 * Stores hue, saturation, lightness, and alpha values. Alpha is optional and defaults to 1. To use the
 * color value, call the toString() method, which returns a formatted HSLA color string.
 *
 */
class HslaColor {
    /**
     * Generates an HslaColor object with random color values.
     * @param alpha Optional alpha value. Defaults to 1.
     * @returns {HslaColor} Returns the randomly generated HslaColor object.
     */
    static randomColor(alpha = 1) {
        let hslaColor = new HslaColor(0, 0, 0, alpha);
        hslaColor.randomize();
        return hslaColor;
    }

    /**
     * Creates an HslaColor object with given color values. Alpha is optional and defaults to 1.
     *
     * @param hue Color value.
     * @param saturation Color saturation.
     * @param lightness Color lightness.
     * @param alpha Transparency.
     */
    constructor(hue = 0, saturation = 100, lightness = 50, alpha = 1) {
        this.hue = hue;
        this.saturation = saturation;
        this.lightness = lightness;
        this.alpha = alpha;
    }

    /**
     * Randomizes the current color values. Does not change alpha value.
     */
    randomize() {
        const maxHue = 360;
        const maxSaturation = 100;
        const maxLightness = 100;
        this.hue = randomInt(maxHue, true);
        this.saturation = randomInt(maxSaturation, true);
        this.lightness = randomInt(maxLightness, true);
    }

    /**
     * Convert to Rgba.
     * @returns {RgbaColor} An RgbaColor object with equivalent color and alpha values.
     */
    toRgba() {
        let redIndex = 0;
        let greenIndex = 1;
        let blueIndex = 2;

        let rgbValues = [3];
        let calcValues = [3];
        const maxRgb = 255;

        let degrees = 60;
        let iterations = 6;
        let halfIterations = iterations / 2;
        let xIndexOffset = 1;

        let s = this.saturation / 100;
        let l = this.lightness / 100;

        let c = (1 - Math.abs(2 * l - 1)) * s;
        let x = c * (1 - Math.abs(this.hue / degrees % 2 - 1));
        let m = l - c / 2;

        for (let i = 0; i < iterations; i++) {
            let minDegrees = i * degrees;
            let maxDegrees = (i + 1) * degrees;

            let cIndex = Math.floor((i + 1) / 2) % halfIterations;
            let xIndex = (xIndexOffset) % halfIterations;
            let zeroIndex = (Math.floor(i / 2) + 2) % halfIterations;

            calcValues[cIndex] = c;
            calcValues[xIndex] = x;
            calcValues[zeroIndex] = 0;

            if (minDegrees <= this.hue && this.hue < maxDegrees) {
                rgbValues[redIndex] = calcValues[redIndex];
                rgbValues[greenIndex] = calcValues[greenIndex];
                rgbValues[blueIndex] = calcValues[blueIndex];
                break;
            }
            xIndexOffset += 2;
        }

        for (let i = 0; i < rgbValues.length; i++) {
            rgbValues[i] = Math.round((rgbValues[i] + m) * maxRgb);
            if (rgbValues[i] > maxRgb) {
                rgbValues[i] = maxRgb;
            }
        }

        return new RgbaColor(rgbValues[redIndex], rgbValues[greenIndex], rgbValues[blueIndex], this.alpha);
    }

    /**
     * Returns a string matching standard HSLA syntax for CSS, HTML, etc.
     * @returns {string} Returns a formatted HSLA string.
     */
    toString() {
        return 'hsla(' + this.hue + ', ' + this.saturation + '%, ' + this.lightness + '%, ' + this.alpha + ')';
    }
}

/**
 * Checks for equality using isEqualCallback. If no function passed in, returns item1 === item2.
 * @param item1 First value.
 * @param item2 Second value.
 * @param isEqualCallback An equality comparison function. Must take two arguments (item1, item2).
 * @returns {*} Returns true if equal.
 */
function isEqual(item1, item2, isEqualCallback = null) {
    if (isFunction(isEqualCallback)) {
        return isEqualCallback(item1, item2);
    }
    return item1 === item2;
}

/**
 * Removes and returns the given element from the array.
 * @param array An array.
 * @param item Item to be removed.
 * @param isEqualCallback Equality function. Takes two items as parameters.
 * @returns {*} Returns the removed item, else null.
 */
function removeFromArray(array, item, isEqualCallback = null) {
    for (let i = 0; i < array.length; i++) {
        let currentItem = array[i];
        if (isEqual(currentItem, item, isEqualCallback)) {
            array.splice(i, 1);
            return currentItem;
        }
    }

    return null;
}

/**
 * Generates a random integer 0 <= x < max.
 * @param max Max range value (exclusive by default).
 * @param isInclusive If true, max is used as an inclusive range value.
 * @returns {number} Returns the randomly generated integer.
 */
function randomInt(max, isInclusive = false) {
    if (isInclusive) {
        max++;
    }

    return Math.floor(Math.random() * Math.floor(max));
}

/**
 * Generates a random float 0 <= x < max.
 * @param max Max range value (exclusive).
 * @returns {number} Returns the randomly generated float.
 */
function randomFloat(max) {
    return Math.random() * max;
}

/**
 * Generates a random integer in the specified range. Default: min <= x < max. Can be adjusted for inclusive or
 * exclusive min and max.
 * @param min Min value (default inclusive).
 * @param max Max value (default exclusive).
 * @param isInclusiveMax If true, max is used as an inclusive range value.
 * @param isInclusiveMin If true, min is used as an inclusive range value.
 * @returns {*} Returns the randomly generated integer.
 */
function randomIntInRange(min, max, isInclusiveMax = false, isInclusiveMin = true) {
    min = Math.ceil(min);
    max = Math.floor(max);

    if (isInclusiveMax) {
        max++;
    }

    if (!isInclusiveMin) {
        min--;
    }

    return Math.floor(Math.random() * (max - min)) + min;
}

/**
 * Generates a random float in the specified range (min <= x < max).
 * @param min Min value (inclusive).
 * @param max Max value (exclusive).
 * @returns {*} Returns the randomly generated float.
 */
function randomFloatInRange(min, max) {
    return Math.random() * (max - min) + min;
}

/**
 * Randomly generates 1 or -1.
 * @returns {number} Returns 1 or -1.
 */
function randomSign() {
    return randomInt(1, true) === 0 ? 1 : -1;
}

/**
 * Randomly generates a boolean value.
 * @returns {boolean} Returns true or false randomly.
 */
function randomBoolean() {
    return randomInt(1, true) === 0;
}

/**
 * Gets the extension from a file path. 'example.txt' returns 'txt'.
 * @param filePath A file path string.
 * @param isIncludePeriod Include period in file (e.g. '.txt').
 */
function fileExtension(filePath, isIncludePeriod = false) {
    let periodIndex = filePath.lastIndexOf('.');

    if (!isIncludePeriod) {
        periodIndex++;
    }

    return filePath.slice(periodIndex, filePath.length);
}

/**
 * Converts degrees to radians.
 * @param degrees Value in degrees.
 * @returns {number} Returns the converted radian value.
 */
function toRadians(degrees) {
    return degrees * Math.PI / HALF_DEGREES;
}

/**
 * Converts radians to degrees.
 * @param angle Value in radians.
 * @returns {number} Returns the converted degree value.
 */
function toDegrees(angle) {
    return angle * HALF_DEGREES / Math.PI;
}

/**
 * Calculates angle in radians.
 * @param x1 First x-value.
 * @param y1 First y-value.
 * @param x2 Second x-value.
 * @param y2 Second y-value.
 * @param isInvertedY Inverts y-axis in calculation. Set true if y increases going down the y-axis from 0.
 * @returns {number} Returns the calculated angle in radians.
 */
function angleRadians(x1, y1, x2, y2, isInvertedY = false) {
    let yModifier = isInvertedY ? -1 : 1;

    let angle = Math.atan2((y2 - y1) * yModifier, x2 - x1);

    if (angle < 0) {
        angle += TWO_PI;
    }

    return angle;
}

/**
 * Utility function to calculate angle in radians. Factors inverted y-axis used by canvas.
 * @param x1 First x-value.
 * @param y1 First y-value.
 * @param x2 Second x-value.
 * @param y2 Second y-value.
 * @returns {number} Returns the calculated angle in radians.
 */
function angleRadiansCanvas(x1, y1, x2, y2) {
    return angleRadians(x1, y1, x2, y2, true);
}

/**
 * Calculates angle in degrees.
 * @param x1 First x-value.
 * @param y1 First y-value.
 * @param x2 Second x-value.
 * @param y2 Second y-value.
 * @returns {number} Returns the calculated angle in degrees.
 */
function angleDegrees(x1, y1, x2, y2) {
    return toDegrees(angleRadians(x1, y1, x2, y2));
}

/**
 * Distance calculation using Pythagorean Theorem.
 * @param x1 First x-value.
 * @param y1 First y-value.
 * @param x2 Second x-value.
 * @param y2 Second y-value.
 * @returns {number} Returns the distance between the two points.
 */
function distance(x1, x2, y1, y2) {
    return Math.hypot(x1 - x2, y1 - y2);
}

/**
 * Creates dx and dy values based on angle and distance input. Factors inverted y-axis used by canvas.
 * @param angle Angle in radians.
 * @param distance Distance value.
 * @param isInvertedY Inverts y-axis in calculation. Set true if y increases going down the y-axis from 0.
 * @returns {{dx: number, dy: number}} Returns dx and dy values.
 */
function calculateDxDy(angle, distance, isInvertedY) {
    let dx = distance * Math.cos(angle);
    let dy = distance * Math.sin(angle);

    if (isInvertedY) {
        dy *= -1;
    }

    return {
        dx: dx,
        dy: dy
    };
}

/**
 * Creates dx and dy values based on angle and distance input. Factors inverted y-axis used by canvas.
 * @param angle Angle in radians.
 * @param distance Distance value.
 * @returns {{dx: number, dy: number}} Returns dx and dy values.
 */
function calculateDxDyCanvas(angle, distance) {
    return calculateDxDy(angle, distance, true);
}

/**
 * Gets a point from a circle's center.
 * @param x Circle x-value.
 * @param y Circle y-value.
 * @param angle Angle in radians
 * @param distance Distance in pixels.
 * @returns {Point} Returns a Point object.
 */
function circlePoint(x, y, angle = 0, distance = 0) {
    let offset = calculateDxDyCanvas(angle, distance);
    return new Point(x + offset.dx, y + offset.dy);
}

/**
 * Determines if an index value is within array bounds.
 * @param array An array.
 * @param index An index value.
 * @returns {boolean} Returns true if index is within array bounds, else false.
 */
function isInBounds1d(array, index) {
    return index >= 0 && index < array.length
}

/**
 * Determines if an index value is within array bounds.
 * @param array An array.
 * @param row Row value.
 * @param col Column value.
 * @returns {boolean} Returns true if index is within array bounds, else false.
 */
function isInBounds2d(array, row, col) {
    return isInBounds1d(array, row) && isInBounds1d(array[0], col)
}

/**
 * Determines if a value is a function.
 * @param value A value.
 * @returns {boolean} Returns true if value is a function, else false.
 */
function isFunction(value) {
    return typeof value === 'function';
}

/**
 * Gets the adjacent neighbor cells of a particular cell in a two-dimensional array. Out-of-bounds indices are not
 * considered. An offset may be specified to search in a ring farther away from the specified cell.
 * @param array A two-dimensional array.
 * @param row Row value.
 * @param col Col value.
 * @param offset Ring offset.
 * @returns {Array} Returns an array of neighbor cells.
 */
function arrayNeighbors(array, row, col, offset = 0) {
    // let iterations = 3;
    let iterations = 3 + offset * 2;
    let neighbors = [];
    for (let i = 0; i < iterations; i++) {
        for (let j = 0; j < iterations; j++) {
            let rowIndex = row - 1 - offset + i;
            let colIndex = col - 1 - offset + j;

            if (isInBounds2d(array, rowIndex, colIndex) && (rowIndex !== row || colIndex !== col)) {
                neighbors.push(array[rowIndex][colIndex]);
            }
        }
    }

    return neighbors;
}

/**
 * Determines if two cells in a two-dimensional array are adjacent.
 * @param row1 First cell row value.
 * @param col1 First cell col value.
 * @param row2 Second cell row value.
 * @param col2 Second cell col value.
 * @returns {boolean} Returns true if the two cells are adjacent, else false.
 */
function isAdjacentArrayCell(row1, col1, row2, col2) {
    return Math.abs(row1 - row2) <= 1 && Math.abs(col1 - col2) <= 1;
}

/**
 * Determines if the passed in value is in the passed in array. Allows custom equality checking with callback function.
 * @param array An array.
 * @param item The item to search for.
 * @param isEqualCallback An equality comparison function. Must take two arguments (item1, item2).
 * @returns {boolean}
 */
function isArrayContains(array, item, isEqualCallback = null) {
    for (let i = 0; i < array.length; i++) {
        let element = array[i];
        if (isEqual(element, item, isEqualCallback)) {
            return true;
        }
    }

    return false;
}

/**
 * Rounds a value to the specified number of decimal places.
 * @param value Value to be rounded.
 * @param decimals Number of decimal places.
 * @returns {number} Returns the rounded number.
 */
function roundDecimals(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

/**
 * Collision detection using axis aligned bounding box comparison. Checks if two rectangles intersect each other. If
 * intersection is detected, a collision is present.
 * @param x1 Rectangle 1 x-value.
 * @param y1 Rectangle 1 y-value.
 * @param width1 Rectangle 1 width.
 * @param height1 Rectangle 1 height.
 * @param x2 Rectangle 2 x-value.
 * @param y2 Rectangle 2 y-value.
 * @param width2 Rectangle 2 width.
 * @param height2 Rectangle 2 height.
 * @returns {boolean} Returns true if the rectangles intersect (collision), else false.
 */
function isAABBCollision(x1, y1, width1, height1, x2, y2, width2, height2) {
    return x1 < x2 + width2 && x1 + width1 > x2 && y1 < y2 + height2 && y1 + height1 > y2;
}

/**
 * Gets the mouse position on the specified canvas element.
 * @param canvas A canvas element.
 * @param event Event.
 * @returns {{x: number, y: number}} Returns the x and y position of the mouse on the canvas element.
 */
function mousePositionCanvas(canvas, event) {
    let rect = canvas.getBoundingClientRect();
    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
}

/**
 * Converts specified number of frame, at particular frame rate, to time in milliseconds.
 * @param frames Number of frame.
 * @param fps Frame rate.
 * @returns {number} Returns the frame' duration in milliseconds.
 */
function framesToMs(frames, fps = 60) {
    return frames / fps * 1000;
}


/**
 * Converts specified duration in milliseconds to frame duration at particular frame rate.
 * @param ms Duration in milliseconds.
 * @param fps Frame rate.
 * @returns {number} Returns the number of frame for the passed in duration in milliseconds.
 */
function msToFrames(ms, fps = 60) {
    return Math.round(ms / 1000 * fps);
}